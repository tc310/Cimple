package pw.h57.cimple.util;

import java.util.Arrays;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * 字符串工具类
 * @author crazy_000
 *
 */
public class StringUtils {
	public static String contactTags(String tagStr){
		String[] tagArr = tagStr.split(",");
		HashSet<String> tagSet = new HashSet<String>();
		for(String s : tagArr){
			s = s.replace(" ", "").trim();
			if(!s.equals("")){
				tagSet.add(s);
			}
		}
		String retStr = Arrays.toString(tagSet.toArray());
		retStr = retStr.substring(1, retStr.length()-1);
		return retStr;
	}
	
	
	public static String cleanHtmlTags(String htmlStr) {
		if(htmlStr == null) {
			return "";
		}
		return Jsoup.clean(htmlStr, Whitelist.none());
	}
}

package pw.h57.cimple.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
/**
 * 发表文章的校验器
 * @author crazy_000
 *
 */
public class BlogValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		// TODO Auto-generated method stub
		validateRequiredString("artTitle", "msg", "请输入标题");
	}

	@Override
	protected void handleError(Controller c) {
		// TODO Auto-generated method stub
		c.keepPara("artTitle","cate","tags","contents");
		c.forwardAction("/admin/article/pub/");
	}

}

package pw.h57.cimple.interceptor;

import java.util.List;

import pw.h57.cimple.entity.Article;
import pw.h57.cimple.entity.Cate;
import pw.h57.cimple.util.StringUtils;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

public class IndexInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		// TODO Auto-generated method stub
		List<Article> tagArt = Article.getAllTag();
		String tagStr = "";
		for(Article art : tagArt){
			System.out.println(art.get("tags"));
			tagStr += art.get("tags") + ",";
		}
		String tags = StringUtils.contactTags(tagStr);
		System.out.println(tags);
		List<Article> newArt = Article.getNewArticle();
		List<Cate> cateLists = Cate.getAllCate();
		ai.getController().setAttr("cateLists", cateLists);
		ai.getController().setAttr("newArt", newArt);
		ai.getController().setAttr("allTags", tags);
		ai.invoke();
	}

}

package pw.h57.cimple.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
/**
 * 登陆的拦截器
 * @author crazy_000
 *
 */
public class LoginInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		// TODO Auto-generated method stub
		String user = ai.getController().getSessionAttr("user");
		if(user == null || user.equals("")){
			ai.getController().redirect("/admin/login");
		} else {
			ai.invoke();
		}
	}

}

package pw.h57.cimple.controller;

import java.util.List;

import pw.h57.cimple.entity.Cate;
import pw.h57.cimple.interceptor.LoginInterceptor;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

/**
 * 分类管理的controller
 * 
 * @author crazy_000
 *
 */
@Before(LoginInterceptor.class)
public class CateController extends Controller {
	public void index() {
		setAttr("title", "分类管理");
		List<Cate> cates = Cate.getAllCate();
		setAttr("cates", cates);
		render("index.html");
	}

	/**
	 * 添加分类action
	 */
	public void addAction() {
		String cateName = getPara("cateName", "");
		String cateAlias = getPara("cateAlias", "");

		if (!cateName.equals("")) {
			if (!Cate.hasCateByName(cateName)) {
				Cate cate = new Cate();
				cate.set("name", cateName);
				if (!cateAlias.equals("")) {
					cate.set("alias", cateAlias);
				} else {
					cate.set("alias", cateName);
				}
				cate.save();
			}
		}
		redirect("/admin/cate");
	}

	public void del() {
		int cateId = getParaToInt(0, -1);
		Cate.delCate(cateId);
		redirect("/admin/cate");
	}
}

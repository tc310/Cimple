package pw.h57.cimple.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import pw.h57.cimple.entity.Article;
import pw.h57.cimple.entity.Cate;
import pw.h57.cimple.interceptor.LoginInterceptor;
import pw.h57.cimple.util.StringUtils;
import pw.h57.cimple.utils.trans.Language;
import pw.h57.cimple.utils.trans.TransUtils;
import pw.h57.cimple.validator.BlogValidator;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

/**
 * 文章部分controller
 * 
 * @author crazy_000
 *
 */
@Before(LoginInterceptor.class)
public class ArticleController extends Controller {
	public void index() {
		int pageNum = getParaToInt(0, 1);
		setAttr("title", "文章管理");
		Page<Article> pageArticle = Article.getArticleByPage(pageNum);
		setAttr("pageArticle", pageArticle);
	}

	/**
	 * 发布文章页面跳转
	 */
	public void pub() {
		List<Cate> cateLists = Cate.getAllCate();
		setAttr("title", "撰写");
		setAttr("cateLists", cateLists);

	}

	@Before(BlogValidator.class)
	public void pubAction() {
		String tagStr = getPara("tags", "");
		int cateId = getParaToInt("cate", -1);
		String articleTitle = getPara("artTitle", "");
		String contents = getPara("contents", "");
		tagStr = StringUtils.contactTags(tagStr);

		Article art = new Article();
		Timestamp time = new Timestamp(System.currentTimeMillis());
		art.set("updateTime", time);
		art.set("title", articleTitle);
		art.set("tags", tagStr);
		art.set("content", contents);
		art.set("cateId", cateId);
		TransUtils utils = new TransUtils("myBlogTrans",
				"qPlGMEwV5mxG3diMGAHj7W+NZwhBuBVJ72svN7AuM6M=");
		String result;
		try {
			result = utils.translate(articleTitle, Language.ChineseSimplified,
					Language.English);
			result = result.replace(",", "-").replace(" ", "-")
					.replace("<", "").replace(">", "").replace("《", "")
					.replace("》", "").replace("（", "").replace("）", "")
					.replace("(", "").replace(")", "").replace("\"", "")
					.replace("“", "").replace("”", "").trim();
			System.out.println(result);
			int transTitleLength = Article.checkTransTitle(result);
			if(transTitleLength > 0) {
				result += "-" + transTitleLength;
			}
			art.set("transTitle", result);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		art.set("isDelete", 0);
		art.save();
		redirect("/admin/article");
	}
	
	
	public void del(){
		int artId = getParaToInt(0, -1);
		Article.dao.findById(artId).set("isDelete", 1).update();
		redirect("/admin/article");
	}
	
	public void modify(){
		int artId = getParaToInt(0, -1);
		setAttr("title", "修改");
		List<Cate> cateLists = Cate.getAllCate();
		setAttr("cateLists", cateLists);
		setAttr("art", Article.dao.findById(artId));
	}
	
	
	public void modifyAction(){
		int artId = getParaToInt("artId", -1);
		String tagStr = getPara("tags", "");
		int cateId = getParaToInt("cate", -1);
		String articleTitle = getPara("artTitle", "");
		String contents = getPara("contents", "");
		tagStr = StringUtils.contactTags(tagStr);

		Article art = Article.dao.findById(artId);
		art.set("title", articleTitle);
		art.set("tags", tagStr);
		art.set("content", contents);
		art.set("cateId", cateId);
		TransUtils utils = new TransUtils("myBlogTrans",
				"qPlGMEwV5mxG3diMGAHj7W+NZwhBuBVJ72svN7AuM6M=");
		String result;
		try {
			result = utils.translate(articleTitle, Language.ChineseSimplified,
					Language.English);
			result = result.replace(",", "-").replace(" ", "-")
					.replace("<", "").replace(">", "").replace("《", "")
					.replace("》", "").replace("（", "").replace("）", "")
					.replace("(", "").replace(")", "").replace("\"", "")
					.replace("“", "").replace("”", "").trim();
			System.out.println(result);
			int transTitleLength = Article.checkTransTitle(result);
			if(transTitleLength > 0) {
				result += "-" + transTitleLength;
			}
			art.set("transTitle", result);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		art.set("isDelete", 0);
		art.update();
		redirect("/admin/article");
	}
	
	
	public void holdOnline(){
		System.out.println("维持在线没有实际意义");
		renderText("");
	}
}

package pw.h57.cimple.controller;

import pw.h57.cimple.entity.Admin;
import pw.h57.cimple.interceptor.LoginInterceptor;
import pw.h57.cimple.util.MD5Util;

import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.core.Controller;
/**
 * 管理员的controller
 * @author crazy_000
 *
 */
@Before(LoginInterceptor.class)
public class AdminController extends Controller {
	
	public void index() {
		// TODO Auto-generated method stub
		setAttr("title", "Cimple后台首页");
	}
	
	@ClearInterceptor
	public void login(){
		setAttr("title", "Cimple后台登陆");
	}
	
	@ClearInterceptor
	public void loginAction(){
		String username = getPara("username", "");
		String pwd = getPara("password", "");
		
		pwd = MD5Util.MD5(pwd);
		System.out.println(pwd);
		int count = Admin.checkUser(username, pwd);
		System.out.println(count + "----------------------");
		if(count == 1){
			setSessionAttr("user", username);
			redirect("/admin");
		} else {
			redirect("/admin/login");
		}
	}
}

package pw.h57.cimple.config;

import org.beetl.ext.jfinal.BeetlRenderFactory;

import pw.h57.cimple.controller.AdminController;
import pw.h57.cimple.controller.ArticleController;
import pw.h57.cimple.controller.CateController;
import pw.h57.cimple.controller.EditorController;
import pw.h57.cimple.controller.IndexController;
import pw.h57.cimple.entity.Admin;
import pw.h57.cimple.entity.Article;
import pw.h57.cimple.entity.Cate;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;

/**
 * jfinal的一些配置信息
 * 
 * @author chrishao
 *
 */
public class BlogConfig extends JFinalConfig {

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("blog.config");
		me.setDevMode(getPropertyToBoolean("devMode", false));
		me.setMainRenderFactory(new BeetlRenderFactory());// 设置渲染引擎为beetl
	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/",IndexController.class);
		// 管理员的路由
		me.add("/admin", AdminController.class);
		me.add("/admin/cate", CateController.class);
		me.add("/admin/article", ArticleController.class);
		me.add("/admin/editor", EditorController.class);
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		DruidPlugin plugin = new DruidPlugin(getProperty("jdbcUrl"),
				getProperty("user"), getProperty("password"));

		me.add(plugin);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(plugin);
		me.add(arp);
		arp.setDevMode(getPropertyToBoolean("devMode", false));
		//以下配置实体类
		arp.addMapping("cate", Cate.class);
		arp.addMapping("admin", Admin.class);
		arp.addMapping("article", Article.class);
		arp.setShowSql(getPropertyToBoolean("devMode", false));
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {

	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		// 添加一个全局的路径handler,用来匹配 根目录
		me.add(new ContextPathHandler("BASE_PATH"));
	}
}

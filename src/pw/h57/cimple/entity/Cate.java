package pw.h57.cimple.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * 分类实体类
 * @author crazy_000
 *
 */
public class Cate extends Model<Cate> {
	public static final Cate dao = new Cate();
	
	public static final List<Cate> getAllCate(){
		List<Cate> cates  = dao.find("select * from cate");
		return cates;
	}
	
	public static final boolean hasCateByName(String name) {
		List<Cate> cates  = dao.find("select id from cate where name = ?",name);
		if(cates.size() > 0) {
			return true;
		}
		return false;
	}
	
	public static final String getCateNameByAlias(String alias) {
		Cate c = dao.findFirst("select name from cate where alias = ?",alias);
		return c.getStr("name");
	}
	
	public static final boolean delCate(int cateId){
		return dao.deleteById(cateId);
	}
	
	public static final long getCateIdByAlias(String alias) {
		Cate c = dao.findFirst("select id from cate where alias = ?",alias);
		return c.getLong("id");
	}
}

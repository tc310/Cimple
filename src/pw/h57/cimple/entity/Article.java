package pw.h57.cimple.entity;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
/**
 * 文章实体类
 * @author crazy_000
 *
 */
public class Article extends Model<Article> {
	public static final Article dao = new Article();
	
	public static List<Article> getNewArticle(){
		return dao.find("select id,transTitle,title,updateTime from article where isDelete = 0 order by id desc limit 10");
	}
	
	public static List<Article> getAllTag(){
		return dao.find("select tags from article where isDelete = 0");
	}
	
	public static Page<Article> getArticleByPage(int pageNum){
		return dao.paginate(pageNum, 10, "select id,title,updateTime", " from article where isDelete = 0 order by id desc");
	} 
	
	public static int checkTransTitle(String transTitle) {
		return dao.find("select id from article where transTitle=?",transTitle).size();
	}
	
	public static Page<Article> getArticleByPageIndex(int pageNum) {
		return dao.paginate(pageNum, 8, "select a.id,a.title,a.content,a.transTitle,a.tags,a.updateTime,c.name", " from article as a,cate c where a.isDelete = 0 and c.id = a.cateId order by a.id desc");
	}
	
	public static Page<Article> getArticleByTag(String tag,int pageNum) {
		return dao.paginate(pageNum, 8, "select a.id,a.title,a.content,a.transTitle,a.tags,a.updateTime,c.name", " from article as a,cate c where a.isDelete = 0 and c.id = a.cateId and tags like ? order by a.id desc","%" + tag + "%");
	}
	
	public static Page<Article> getArticleByCateId(long cateId,int pageNum) {
		return dao.paginate(pageNum, 8, "select a.id,a.title,a.content,a.transTitle,a.tags,a.updateTime,c.name", " from article as a,cate c where a.isDelete = 0 and c.id = a.cateId and a.cateId = ? order by a.id desc",cateId);
	}
	
	public static Article getArticleByTransTitle(String transTitle) {
		return dao.findFirst("select * from article where transTitle = ?",transTitle);
	}
	
	public static Article getArticleById(String id){
		return dao.findById(id);
	}
}

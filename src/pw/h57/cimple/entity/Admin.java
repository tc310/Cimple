package pw.h57.cimple.entity;

import com.jfinal.plugin.activerecord.Model;

public class Admin extends Model<Admin> {
	public static final Admin dao = new Admin();
	
	public static int checkUser(String username, String md5pwd){
		return dao.find("select * from admin where username=? and pwd=?",username,md5pwd).size();
	}
}
